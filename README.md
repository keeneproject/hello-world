# Hello World in Docker

This example project demonstrates how to build an application in a container using GitLab CI. We're building a container that runs a Python application, which in turn executes a compiled C program.

**GitLab CI will:**

* Build the app in a container.
* Run the container.
* Push the container to this project's container registry.
* Run `testscript.sh` inside the built container.
* Scan the container for vulnerabilities.
* Deploy the container.

## File Descriptions

* **Dockerfile:** Builds the application into a container.
* **testscript.sh:** A simple Bash script to perform some tests.
* **.gitlab-ci.yml:** The GitLab CI pipeline that builds, tests, and deploys the application.
* **mypythonapp.py:** The Python application.
* **hello.c:** The C application.
* **bad-code-examples:**  Examples of bad code to test vulnerability scanning.
 

## How to Build This Project Locally
1. Clone the repo
```
git clone https://gitlab.com/keeneproject/example-projects/hello-world-docker.git
```
2. Build the container and run it
```
cd hello-world-docker
docker build -t helloworld-app .
docker run --rm helloworld-app:latest
```

## Useful links
https://docs.docker.com/get-started/
https://docs.gitlab.com/ee/ci/quick_start/

