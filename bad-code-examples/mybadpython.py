# Python program with bad code
from flask import request

@app.route('/')
def index():
    module = request.args.get("module")
    exec("import urllib%s as urllib" % module) # Noncompliant

def foo(a):  # NonCompliant
    b = 12
    if a == 1:
        return b
    return b
