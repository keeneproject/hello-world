import sys
import subprocess
import os

# Make sure we are running python3+
if sys.version_info[0] < 3:
    raise Exception("Python 3 or a more recent version is required.")

print("Running Pascal's Triangle\n")
# The following prints Pascal's Triangle
rows = 6
coef = 1

for i in range(1, rows+1):
    for space in range(1, rows-i+1):
        print(" ",end="")
    for j in range(0, i):
        if j==0 or i==0:
            coef = 1
        else:
            coef = coef * (i - j)//j
        print(coef, end = " ")
    print()

print("\nExecuting our C program")
subprocess.call(f"./hello")
